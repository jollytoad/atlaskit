import Avatar from '@atlaskit/avatar';

import OldAppSwitcherIcon from '@atlaskit/icon/glyph/app-switcher';
import OldArrowRightLongIcon from '@atlaskit/icon/glyph/arrow-right-long';
import OldCancelIcon from '@atlaskit/icon/glyph/cancel';
import OldConfirmIcon from '@atlaskit/icon/glyph/confirm';
import OldExpandIcon from '@atlaskit/icon/glyph/expand';
import OldHelpIcon from '@atlaskit/icon/glyph/help'; // trailing comment
import OldHintIcon from '@atlaskit/icon/glyph/hint';
import OldMediaUploaderIcon from '@atlaskit/icon/glyph/hipchat/media-uploader';
import OldLogInIcon from '@atlaskit/icon/glyph/log-in';
import OldSquareIcon from '@atlaskit/icon/glyph/media-services/square';
import OldRemoveIcon from '@atlaskit/icon/glyph/remove';
import OldScheduleAddIcon from '@atlaskit/icon/glyph/schedule-add';
import OldTimeIcon from '@atlaskit/icon/glyph/time';

const myOtherVar = true;
