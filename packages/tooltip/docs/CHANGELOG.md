# @atlaskit/tooltip

## 6.0.0 (2017-08-30)

* breaking; The tooltip trigger is now wrapped in a div with 'display: inline-block' applied. Previously it was ([de263e5](https://bitbucket.org/atlassian/atlaskit/commits/de263e5))
* breaking; tooltip now disappears as soon as the mouse leaves the trigger (issues closed: ak-1834) ([de263e5](https://bitbucket.org/atlassian/atlaskit/commits/de263e5))

## 5.0.1 (2017-08-21)

* bug fix; fix PropTypes warning ([040d579](https://bitbucket.org/atlassian/atlaskit/commits/040d579))
## 5.0.0 (2017-08-11)

* bug fix; fix the theme-dependency ([db90333](https://bitbucket.org/atlassian/atlaskit/commits/db90333))

* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* feature; updated dark colors for Tooltip ([8fbbb8c](https://bitbucket.org/atlassian/atlaskit/commits/8fbbb8c))






* feature; new theme methods ([3656ee3](https://bitbucket.org/atlassian/atlaskit/commits/3656ee3))


* feature; add dark mode support to tooltip ([aa87b89](https://bitbucket.org/atlassian/atlaskit/commits/aa87b89))
## 4.0.0 (2017-08-11)

* breaking; affects internal styled-components implementation ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* breaking; implement dark mode theme ([d14522a](https://bitbucket.org/atlassian/atlaskit/commits/d14522a))
* feature; updated dark colors for Tooltip ([8fbbb8c](https://bitbucket.org/atlassian/atlaskit/commits/8fbbb8c))






* feature; new theme methods ([3656ee3](https://bitbucket.org/atlassian/atlaskit/commits/3656ee3))


* feature; add dark mode support to tooltip ([aa87b89](https://bitbucket.org/atlassian/atlaskit/commits/aa87b89))

## 3.4.2 (2017-07-27)


* fix; rename jsnext:main to jsnext:experimental:main temporarily ([c7508e0](https://bitbucket.org/atlassian/atlaskit/commits/c7508e0))

## 3.4.1 (2017-07-25)


* fix; use class transform in loose mode in babel to improve load performance in apps ([fde719a](https://bitbucket.org/atlassian/atlaskit/commits/fde719a))

## 3.1.0 (2017-07-17)

## 3.1.0 (2017-07-17)

## 3.1.0 (2017-07-17)


* fix; rerelease, failed prepublish scripts ([5fd82f8](https://bitbucket.org/atlassian/atlaskit/commits/5fd82f8))

## 3.1.0 (2017-07-17)


* feature; added ES module builds to dist and add jsnext:main to most ADG packages ([ea76507](https://bitbucket.org/atlassian/atlaskit/commits/ea76507))

## 2.0.0 (2017-07-17)


null replace LESS with SC ([d1b5911](https://bitbucket.org/atlassian/atlaskit/commits/d1b5911))


* breaking; named export "Tooltip" is now "TooltipStateless". prop "visible" is now "isVisible"

ISSUES CLOSED: AK-2059

## 1.2.1 (2017-07-13)


* fix; add prop-types as a dependency to avoid React 15.x warnings ([92598eb](https://bitbucket.org/atlassian/atlaskit/commits/92598eb))

## 1.2.0 (2017-05-10)


* feature; bump layer version in [@atlaskit](https://github.com/atlaskit)/tooltip ([cfa9903](https://bitbucket.org/atlassian/atlaskit/commits/cfa9903))

## 1.1.2 (2017-04-27)


* fix; update legal copy to be more clear. Not all modules include ADG license. ([f3a945e](https://bitbucket.org/atlassian/atlaskit/commits/f3a945e))

## 1.1.1 (2017-04-26)


* fix; update legal copy and fix broken links for component README on npm. New contribution and ([0b3e454](https://bitbucket.org/atlassian/atlaskit/commits/0b3e454))

## 1.1.0 (2017-04-20)


* feature; removed explicit style! imports, set style-loader in webpack config ([891fc3c](https://bitbucket.org/atlassian/atlaskit/commits/891fc3c))

## 1.0.8 (2017-04-04)


* fix; adds defensive code to allow testing in mocha/jsdom ([2eaab5b](https://bitbucket.org/atlassian/atlaskit/commits/2eaab5b))

## 1.0.6 (2017-03-21)

## 1.0.6 (2017-03-21)


* fix; maintainers for all the packages were added ([261d00a](https://bitbucket.org/atlassian/atlaskit/commits/261d00a))

## 1.0.5 (2017-03-06)

## 1.0.4 (2017-02-28)


* fix; prevent word wrapping of tooltip for TextAdvancdFormatting elements ([31b51a4](https://bitbucket.org/atlassian/atlaskit/commits/31b51a4))
* fix; removes jsdoc annotations and moves content to usage.md ([2d794cd](https://bitbucket.org/atlassian/atlaskit/commits/2d794cd))
* fix; dummy commit to release stories ([3df5d9f](https://bitbucket.org/atlassian/atlaskit/commits/3df5d9f))

## 1.0.3 (2017-02-20)


* Add missing TS definition for tooltip ([aae714d](https://bitbucket.org/atlassian/atlaskit/commits/aae714d))
* Add TS definition for tooltip ([5c023e9](https://bitbucket.org/atlassian/atlaskit/commits/5c023e9))
* Use atlaskit tooltips instead of browser native tooltips ([d0018eb](https://bitbucket.org/atlassian/atlaskit/commits/d0018eb))

## 1.0.2 (2017-02-07)


* fix; Updates package to use scoped ak packages ([73d1427](https://bitbucket.org/atlassian/atlaskit/commits/73d1427))
