/* tslint:disable:variable-name */
import styled from 'styled-components';
import { akColorN30 } from '@atlaskit/util-shared-styles';
import {borderRadius} from '../../styles';

export const Wrapper = styled.span`
  ${borderRadius}
  background-color: ${akColorN30};
`;
